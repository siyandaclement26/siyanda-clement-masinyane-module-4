import 'package:flutter/material.dart';
import 'package:flutter_application_1/profile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'My New App',
      home: Profile(),
      theme: ThemeData(
        primarySwatch: Colors.brown,
        accentColor: Colors.grey,
        scaffoldBackgroundColor: Color.fromARGB(255, 52, 82, 95),
      ),
    );
  }
}
