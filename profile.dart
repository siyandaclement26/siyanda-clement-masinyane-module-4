import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile'),
        centerTitle: true,
      ),
      body: Column(children: [
        SizedBox(
          height: 115,
          width: 115,
          child: Stack(
            fit: StackFit.expand,
            clipBehavior: Clip.none,
            children: [
              CircleAvatar(
                backgroundImage: AssetImage("assets/siyanda.png"),
              )
            ],
          ),
        ),
        // ignore: prefer_const_constructors
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: const TextField(
            obscureText: false,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'username',
              hintText: 'Enter UserName',
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: const TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'password',
              hintText: 'Enter Password',
            ),
          ),
        ),
        // ignore: prefer_const_constructors
        SizedBox(
          height: 115,
          width: 115,
          child: Center(child: Lottie.asset('assets/learning.json')),
        ),
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          //Add your press here!
        },
        child: const Icon(Icons.edit),
      ),
    );
  }
}
